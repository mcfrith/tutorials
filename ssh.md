# ssh

`ssh` is the main way to access remote computers from a command line:

    ssh myRemoteComputer

Then you'll be logged into `myRemoteComputer`, using its command line.
To log out, do `exit` or `Ctrl-d`, and you'll be back in your local
computer's command line.

## Setup

`ssh` can work in a few different ways, but the usual way (in my
experience) is [public-key cryptography][].  To set this up, the first
step is to generate a public/private key pair on your local computer.
The currently (2022) recommended way to do that is:

    ssh-keygen -t ed25519

The `-t ed25519` voodoo requests the currently-fashionable
cryptographic method.  (If you omit `-t ed25519`, it'll use an older
method.)  Of course I haven't memorized this "ed number": I look up
how to run `ssh-keygen` on the fairly rare occasions when I use it.

Anyway, `ssh-keygen` will ask you to choose a "passphrase": choose a
good one (how to do that is beyond the scope of this document).
Finally, it will create two files in a `.ssh` directory in your home
directory, have a look: `ls ~/.ssh`

* `id_ed25519` is the "private key": don't give this to anyone, don't
  touch it, don't do anything with it.
* `id_ed25519.pub` is the "public key", which you can freely share
  with the world.

The next step is to give the public key (*not the private key!*) to
whoever manages the remote computer, and hope they install it for you.
Then you should be able to ssh into it with something like:

    ssh myUserName@remote.computer.edu.jp

where `myUserName` is your user-name on the remote computer.  If it's
the same as your user-name on your local computer, you can omit the
`myUserName@`.

## Abbreviating

Of course it's unacceptable to have to type
`myUserName@remote.computer.edu.jp` each time, so let's fix that.  In
your local computer's `.ssh` directory, put these lines into a file
called `config`:

    Host budgie
    Hostname remote.computer.edu.jp

This makes `budgie` a nickname for `remote.computer.edu.jp`, so you
can do:

    ssh myUserName@budgie

To automate the user-name, add one more line:

    Host budgie
    Hostname remote.computer.edu.jp
    User myUserName

So you can just do:

    ssh budgie

`ssh` will refuse to use `config` if anyone except you has write
permission for `config`.  If this problem happens, you can fix the
permissions:

    chmod 644 ~/.ssh/config

## Copying files

After setting up ssh, you can copy files using `scp`, which mostly
works like `cp`.  For example, if you do this (on your local
computer), it copies `myFile` from your local computer into
`some/directory/` on the remote computer `budgie`:

    scp myFile budgie:some/directory/

If you do this (on your local computer), it copies `some/file` from
`budgie` into your current directory on your local computer:

    scp budgie:some/file .

Here `.` means "current directory".

## Avoiding the passphrase

Depending on your setup, you may have to type the passphrase each time
you do `ssh` or `scp`, which is unacceptably tedious.  It turns out
there's a standard way to mostly avoid that.  The passphrase decrypts
the private key, and it's possible to then store the decrypted key in
the memory of an "ssh agent": so you won't need to type the passphrase
again till you log out.  (Apparently, this is considered more secure
than simply not having a passphrase.)

How to set this up might vary between computers, but this might work.
Add these lines to the end of the `config` file on your local
computer:

    Host *
    AddKeysToAgent yes

The `Host *` means that this applies to all ssh destinations.

## Avoiding ssh connections "dying"

You may find that an ssh connection weirdly "dies" if you don't use it
for a while.  This can sometimes be fixed by adding another line to
`config`:

    Host budgie
    Hostname remote.computer.edu.jp
    User myUserName
    ServerAliveInterval 60

This makes your local computer contact the remote computer every 60
seconds.  I don't really understand this, but a random person on the
internet says "this is to prevent intermediate routers and firewalls
from thinking a session is idle, and dropping it".  And I'm not
certain that `60` is a good value.

## Installing the public key yourself

As mentioned above, to set up ssh, the public key needs to be
"installed" on the remote computer.  Let's see how to do that.

First, you have to copy the public key to the remote computer, but
how?  You might be able to use `scp` via intermediate computers.  Or
copy it with a USB memory stick.  Or I've found this very useful:
[magic wormhole][].

To "install" the public key, add it to a file called `authorized_keys`
in the `.ssh` directory of the remote computer:

    cat my-key.pub >> ~/.ssh/authorized_keys

It's possible for `authorized_keys` to have more than one key, if you
want to ssh from more than one computer.

## Indirect ssh

Sometimes, security bureaucrats set things up so you can't ssh
directly to a computer (let's call it `parrot`), but you can connect
to an intermediate computer (e.g. `budgie`), and from there to
`parrot`.  You can automate this by adding a "ProxyJump" line to
`config` on your local computer:

    Host parrot
    Hostname parrot.bureaucrats.org
    ProxyJump budgie

Then, if your public key (from your local computer) is installed on
both `budgie` and `parrot`, you can just do:

    ssh parrot

Take that, bureaucrats!

[public-key cryptography]: https://en.wikipedia.org/wiki/Public-key_cryptography
[magic wormhole]: https://magic-wormhole.readthedocs.io/
