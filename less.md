# less

`less` is a nice command-line tool for looking at "text" files (any
kind of text: tables of data, DNA sequences, etc.)  It has lots of
useful features that I didn't know for a long time - maybe this will
help you learn them sooner.

To to look at `myNiceFile`, do:

    less myNiceFile

That's nice, but **the most important thing is, how to quit??**  Press `q`.

## Moving up and down

If the file is longer than your screen, you can use the down arrow to
move down in the file, and the up arrow to go back up.

* `Space` (or `f` for "forward"): moves down 1 screenful.
* `b` (for "backward") moves up 1 screenful.
* `d` (for "down") moves down by half a screen.
* `u` (for "up") moves up by half a screen.
* `<` (or `g`, I didn't know that) moves to the start of the file.
* `>` (or `G`, I didn't know that) moves to the end of the file.

## Long lines

If the file has lines that are wider than your screen, `less` will
"wrap" them onto the next line by default.  This often looks terrible,
if it's e.g. a table of data, or a sequence alignment.  To fix that, do:

    less -S myNiceFile

Then it cuts long lines at the screen edge, and you can use the right
and left arrow keys to move right and left.

It's inconvenient that you have to decide to use `-S` in advance, *but
you don't have to!*  When you're already using `less`, you can just
type `-S` and it has the same effect!  And you can turn it off by
typing `-S` again.

*And how did I not know these before now?*

* If you type (say) `5` then right arrow, it will move right by 5
  characters.  From then on, the right and left arrows will move by 5
  characters.  (Also `-#5` sets the shift to 5 characters, and `-#0`
  sets it to the default, which is half the screen width.)

* `Alt-}` jumps to the end of the longest displayed line, and `Alt-{`
  jumps back to the start (not in old versions of `less`).

## Searching

To search in `less`, press `/`, then type something you want to search
for and press `Enter`.  It will highlight matches and jump to the
first one.  You can then press `n` to jump to the next matches, and
`N` to go back to previous matches.

A confusing thing is that the search uses "regular expressions", which
means that some symbols like `*` and `.` have special meanings.  *You
can turn off regular expressions* by doing `Ctrl-r` just after `/`.

The search is case-sensitive, but I often (usually?) want
case-insensitive, which I can get like this:

    less -i myNiceFile

*But I never remember to do that:* instead, I type `-i` after starting
`less`, which has the same effect (and another `-i` toggles it off).
Actually, `-i` makes it case-insensitive *unless* the thing you search
for includes any uppercase letters: I love it.

`Alt-u` turns off the highlighting.

`?` is the same as `/`, except that it searches backwards.

Some things I didn't know:

* Just after `/`, `Ctrl-n` or `!` finds lines that **don't** match.
* `&` is like `/`, but it hides non-matching lines!  (`&` followed by
  `Enter` turns this off.)
* `-j3` (for example) makes searches jump to matches so they're on the
  3rd line of your screen, instead of the 1st line.

## Miscellaneous

* `-M`: show line numbers at the bottom.
* To go to (say) line number 99, type `99g`.
* If the file contains TABs, you can do (e.g.) `-x9` to set the tab
  width to 9.  This can help to line up the columns of tab-delimited
  tables.  Or you can line up the columns automatically with the
  `column` command: `column -t myNiceFile | less`

I didn't know these:

* `-N`: show the line number of each line.
* `-s`: squeeze consecutive blank lines into one blank line.
