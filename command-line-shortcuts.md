# Command-line keyboard shortcuts

These are some shortcuts I've found handy for using the command line.

## Tab

The most important one is the `Tab` key.  If I need to type
`cd stupidLongDirectoryName`, I don't want to type the whole thing,
that would be terrible!  Instead I type something like `cd stu` and
hit `Tab`, which automatically fills in the rest!  This only works if
there's nothing else starting with `stu`.

## Arrow keys

While you're typing a command, you can use the left and right arrow
keys to move back and forth, so you can edit things in the middle.

If you press the up arrow, your previous command appears: so you can
repeat or edit it.  If you press up arrow twice, you get your 2nd-last
command, and so on.  The down arrow moves back down your command
history.  So you can use the up and down arrows to browse your
previous commands.

Amazing fact: up arrow can even get commands from before you restarted
your computer (because they're saved in a file).

## Jumping left/right

Moving a long way with left/right arrow keys gets really sloooow.
`Ctrl-a` jumps to the beginning of the line, and `Ctrl-e` jumps to the
end: I use these a lot.

## Miscellaneous editing

`Ctrl-k` deletes everything from the cursor position to the end of the
line.  (Why?  Is this particular kind of delete especially useful?  I
don't know.  But sometimes I find it useful.)

`Alt-.` enters the last "word" from your previous command.  I didn't
know this for the longest time, but now I use it a lot (I guess
because I often want to run another command on the same file?)

*Wait what is this* `Alt`?  It varies confusingly between computers.
On a Mac: in your Terminal preferences, you need to find and set
something like "Use Option as Meta key".  Then you can use the
"option" key as what we're calling `Alt`.

## Miscellaneous effects

`Ctrl-l` (that's letter "ell", not number "one") clears the screen.
This sometimes helps to see things clearly without clutter.

`Ctrl-d` means "end of input".  It makes the command line quit.

If you're running a program that's taking a long time, `Ctrl-c` tells
it to quit.  (Actually, some programs ignore `Ctrl-c`.)

## Searching your command history

It took me a long time to appreciate this (so maybe it's not for
beginners, or maybe it's just me), but I love it now!  Do you
sometimes press the up arrow many times to recall a command from a
while back?  Tedious, isn't it?  What you need is...

`Ctrl-r`, then type a small part of the command you're trying to find.
This retrieves the most recent command that contains what you type.
It updates the retrieval "in real time" as you type, which totally
confused me at first, but it's great.

If you then press `Ctrl-r` again (and again), it gets the 2nd-last
(3rd-last) command that contains what you typed.  `Ctrl-s` moves back
down these commands, so you can use `Ctrl-r` and `Ctrl-s` to browse
them.

You can quit the search with `Ctrl-g`, or else edit/run the command.

Wow, I didn't know *this!*  `Ctrl-o` runs the command, just like
pressing `Enter`, *and then recalls the next command in the history*.
That's wonderful because I often want to repeat a series of commands.

**Warning:** `Ctrl-s` might "not work as expected", see next section...

## Crazy stuff

So there's a standard shortcut for "completely freeze the command
line", which you might do by mistake.  Madness.  This is `Ctrl-s`, but
actually the effect of `Ctrl-s` depends on your computer's setup.  If
you've frozen yourself with `Ctrl-s`, you can unfreeze with `Ctrl-q`.

Another scary one: if you're running a program, e.g. an editor where
you're editing a file, and you accidentally do `Ctrl-z`, it suddenly
quits!  Actually, `Ctrl-z` suspends the program, and you'll see
something like this:

    [1]+  Stopped

You can un-suspend it by doing

    fg

which means continue running it "in the foreground".
